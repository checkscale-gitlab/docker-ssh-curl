target "default" {
    inherits = ["3.14"]
}

variable "IMAGE" {
    default=""
}

group "release" {
    targets = [
        "3.14-release",
        "3.13-release",
        "3.12-release",
        "3.11-release"
    ]
}

target "platforms" {
    platforms = [
        "linux/amd64",
        "linux/arm/v6",
        "linux/arm/v7",
        "linux/arm64",
        "linux/386",
    ]
}

target "gitlab-scan" {
    inherits = ["3.14"]
    tags = [
        "${IMAGE}"
    ]
}

target "3.14" {
    dockerfile = "Dockerfile"
    args = {
        ALPINE_VERSION = "14"
    }
    tags = [
        "cyb3rjak3/docker-curl-ssh:3.14",
        "cyb3rjak3/docker-curl-ssh:latest",
        "registry.gitlab.com/cyb3r-jak3/docker-ssh-curl:3.14",
        "registry.gitlab.com/cyb3r-jak3/docker-ssh-curl:latest"
    ]
}
target "3.13" {
    dockerfile = "Dockerfile"
    args = {
        ALPINE_VERSION = "13"
    }
    tags = [
        "cyb3rjak3/docker-curl-ssh:3.13",
        "registry.gitlab.com/cyb3r-jak3/docker-ssh-curl:3.13"
    ]
}
target "3.12" {
    dockerfile = "Dockerfile"
    args = {
        ALPINE_VERSION = "12"
    }
    tags = [
        "cyb3rjak3/docker-curl-ssh:3.12",
        "registry.gitlab.com/cyb3r-jak3/docker-ssh-curl:3.12"
    ]
}
target "3.11" {
    dockerfile = "Dockerfile"
    args = {
        ALPINE_VERSION = "11"
    }
    tags = [
        "cyb3rjak3/docker-curl-ssh:3.11",
        "registry.gitlab.com/cyb3r-jak3/docker-ssh-curl:3.11"
    ]
}

target "3.14-release" {
    inherits = ["3.14", "platforms"]
}
target "3.13-release" {
    inherits = ["3.13", "platforms"]
}
target "3.12-release" {
    inherits = ["3.12", "platforms"]
}
target "3.11-release" {
    inherits = ["3.11", "platforms"]
}